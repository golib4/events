package events

import (
	"fmt"
	"gitlab.com/golib4/events/events/storage"
	"gitlab.com/golib4/logger/logger"
	"time"
)

type EventSubscriber interface {
	Subscribe(topic string, callback func(payload []byte) error)
}

type dbEventSubscriber struct {
	storage *storage.Storage
	logger  logger.Logger
}

func NewDbEventSubscriber(
	storage *storage.Storage,
	logger logger.Logger,
) EventSubscriber {
	return &dbEventSubscriber{
		storage: storage,
		logger:  logger,
	}
}

func (s dbEventSubscriber) Subscribe(topic string, callback func(payload []byte) error) {
	for {
		err := s.processUnpublishedEvents(topic, callback)
		if err != nil {
			s.logger.Error(err.Error())
		}

		time.Sleep(time.Second * 1)
	}
}

func (s dbEventSubscriber) processUnpublishedEvents(topic string, callback func(payload []byte) error) error {
	count, err := s.storage.GetCount(topic)
	if err != nil {
		return fmt.Errorf("get count error: %s", err.Error())
	}

	const pageSize = 1000
	var offset int
	for offset < count {
		limit := pageSize
		if count-offset < pageSize {
			limit = count - offset
		}

		list, err := s.storage.Get(topic, uint(limit), uint(offset))
		if err != nil {
			return fmt.Errorf("get list error: %s", err.Error())
		}
		for _, event := range list {
			err := callback(event.Payload)
			if err != nil {
				s.logger.Error(fmt.Sprintf("can not process event: %s", err))
				continue
			}

			err = s.storage.SetProcessed(event.UniqueIdentifier)
			if err != nil {
				s.logger.Error(fmt.Sprintf("can not set event processed: %s", err))
			}
		}

		offset += pageSize
	}

	return nil
}
