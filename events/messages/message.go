package messages

import "time"

type Message interface {
	GetTopic() string
	GetUniqueIdentifier() string
	GetCreatedAt() time.Time
	GetPayload() any
}
