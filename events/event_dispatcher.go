package events

import (
	"fmt"
	"gitlab.com/golib4/events/events/messages"
	"gitlab.com/golib4/events/events/storage"
)

type EventDispatcher interface {
	Publish(message messages.Message) error
}

type dbEventDispatcher struct {
	storage *storage.Storage
}

func NewDbEventDispatcher(storage *storage.Storage) EventDispatcher {
	return dbEventDispatcher{storage: storage}
}

func (d dbEventDispatcher) Publish(message messages.Message) error {
	err := d.storage.Create(message)
	if err != nil {
		return fmt.Errorf("can not save event message: %v", err)
	}
	return nil
}
