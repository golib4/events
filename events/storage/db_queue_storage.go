package storage

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/doug-martin/goqu/v9"
	"gitlab.com/golib4/events/events/messages"
)

type Storage struct {
	connection *sql.DB
}

func NewStorage(connection *sql.DB) Storage {
	return Storage{connection: connection}
}

const tableName = "queue"

func (s Storage) Create(message messages.Message) error {
	payload, err := json.Marshal(message.GetPayload())
	if err != nil {
		return fmt.Errorf("marshal payload error: %v", err)
	}
	query, _, err := goqu.Insert(tableName).Rows(goqu.Record{
		"created_at":        message.GetCreatedAt(),
		"topic":             message.GetTopic(),
		"payload":           payload,
		"processed":         false,
		"unique_identifier": message.GetUniqueIdentifier(),
	}).ToSQL()
	if err != nil {
		return fmt.Errorf("generate query error: %v", err)
	}

	_, err = s.connection.Exec(query)
	if err != nil {
		return fmt.Errorf("exec query error: %v", err)
	}
	return nil
}

func (s Storage) GetCount(topic string) (int, error) {
	query, _, err := goqu.Select(goqu.COUNT("*")).From(tableName).Where(goqu.Ex{
		"topic":     topic,
		"processed": false,
	}).ToSQL()
	if err != nil {
		return 0, fmt.Errorf("generate query error: %v", err)
	}
	row := s.connection.QueryRow(query)
	var count int
	err = row.Scan(&count)
	if err != nil {
		return 0, fmt.Errorf("can not execute query: %s", err)
	}

	return count, nil
}

func (s Storage) Get(topic string, limit uint, offset uint) ([]QueueMessage, error) {
	query, _, err := goqu.Select("topic", "created_at", "payload", "unique_identifier").From(tableName).Where(goqu.Ex{
		"topic":     topic,
		"processed": false,
	}).Order(
		goqu.I("created_at").Asc(),
	).Limit(limit).Offset(offset).ToSQL()
	if err != nil {
		return nil, fmt.Errorf("generate query error: %v", err)
	}
	rows, err := s.connection.Query(query)
	if err != nil {
		return nil, fmt.Errorf("can not execute query: %s", err)
	}

	models, err := s.mapTableIntoModel(rows)
	if err != nil {
		return nil, fmt.Errorf("can not map table into model: %s", err)
	}

	return models, nil
}

func (s Storage) SetProcessed(uniqueIdentifier string) error {
	query, _, err := goqu.Update(tableName).Set(goqu.Record{
		"processed": true,
	}).Where(goqu.Ex{
		"unique_identifier": uniqueIdentifier,
	}).ToSQL()

	if err != nil {
		return fmt.Errorf("generate query error: %v", err)
	}

	_, err = s.connection.Exec(query)
	if err != nil {
		return fmt.Errorf("exec query error: %v", err)
	}
	return nil
}

func (s Storage) mapTableIntoModel(rows *sql.Rows) ([]QueueMessage, error) {
	var models []QueueMessage

	for rows.Next() {
		model := QueueMessage{}

		err := rows.Scan(
			&model.Topic,
			&model.CreatedAt,
			&model.Payload,
			&model.UniqueIdentifier,
		)

		if err != nil {
			return nil, fmt.Errorf("can not scan db rows: %s", err)
		}

		models = append(models, model)
	}

	return models, nil
}
