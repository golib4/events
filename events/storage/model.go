package storage

import "time"

type QueueMessage struct {
	Topic            string
	CreatedAt        time.Time
	Payload          []byte
	UniqueIdentifier string
}
