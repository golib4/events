module gitlab.com/golib4/events

go 1.22.2

require (
	github.com/doug-martin/goqu/v9 v9.19.0
	gitlab.com/golib4/logger v1.0.6
)

require (
	github.com/josharian/intern v1.0.0 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/olivere/elastic/v7 v7.0.32 // indirect
	github.com/pkg/errors v0.9.1 // indirect
)
